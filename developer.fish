function sugar-developer-enable -d 'Set module repository to git.'
  set -l module_info (string split ':' $argv[1])
  set -l module_repository $module_info[1]
  set -l module_name $module_info[2]
  set -l module_path $sugar_install_directory/modules/$module_repository/$module_name

  git -C $module_path remote set-url origin git@gitlab.com:$module_repository/$module_name.git
end

function sugar-developer-disable -d 'Set module repository to https.'
  set -l module_info (string split ':' $argv[1])
  set -l module_repository $module_info[1]
  set -l module_name $module_info[2]
  set -l module_path $sugar_install_directory/modules/$module_repository/$module_name

  git -C $module_path remote set-url origin https://gitlab.com/$module_repository/$module_name.git
end

function sugar-developer-cd -d 'Change directory to module directory.'
  set -l module_info (string split ':' $argv[1])
  set -l module_repository $module_info[1]
  set -l module_name $module_info[2]
  set -l module_path $sugar_install_directory/modules/$module_repository/$module_name

  cd $module_path
end

function sugar-developer-init -d 'Initialize a new module.'
  set -l module_info (string split ':' $argv[1])
  set -l module_repository $module_info[1]
  set -l module_name $module_info[2]
  set -l module_path $sugar_install_directory/modules/$module_repository/$module_name

  mkdir -p $module_path
  cd $module_path
  git init
  git remote add origin git@gitlab.com:$module_repository/$module_name.git
  touch $module_name.fish
  git add $module_name.fish
end

function sugar-developer-modified -d "Exits zero if the module has been modified."
  set -l module_info (string split ':' $argv[1])
  set -l module_repository $module_info[1]
  set -l module_name $module_info[2]
  set -l module_path $sugar_install_directory/modules/$module_repository/$module_name

  set -l module_modified false

  # exits non-zero if there are changes
  git -C $module_path diff --exit-code --no-patch

  if test ! $status -eq 0
    set module_modified true
  end

  # exits zero if there are untracked files
  git -C $module_path ls-files --other --exclude-standard | \
    grep '.*' > /dev/null 2>&1

  if test $status -eq 0
    set module_modified true
  end

  if $module_modified
    return 0
  end
  return 1
end

function sugar-developer-modules -d "Returns a space separated list of all modules."
  set -l modules
  for path in $sugar_install_directory/modules/**
    set -l base (basename $path)
    if contains $base.fish (ls $path)
      set -l module (string match --groups-only --regex "modules/(.*)\$" $path)
      set -l module_path (dirname $module)
      set -l module_name (basename $module)
      set -a modules $module_path:$module_name
    end
  end
  echo $modules
end

function sugar-developer-status -d 'Display the git status of a module.'
  set -l module_info (string split ':' $argv[1])
  set -l module_repository $module_info[1]
  set -l module_name $module_info[2]
  set -l module_path $sugar_install_directory/modules/$module_repository/$module_name

  sugar-developer-modified $argv[1]

  if test $status -eq 0
    sugar-message bold "$module_repository:$module_name"
    git -C $module_path status --short
  end
end

function sugar-developer-status-all -d 'Display the git status of all installed modules.'
  for module in (string split " " (sugar-developer-modules))
    set -l module_info (string split ":" $module)
    set -l module_repository $module_info[1]
    set -l module_name $module_info[2]
    sugar-developer-status $module_repository:$module_name
  end
end

function sugar-developer-diff -d 'Display a git diff of a module.'
  set -l module_info (string split ':' $argv[1])
  set -l module_repository $module_info[1]
  set -l module_name $module_info[2]
  set -l module_path $sugar_install_directory/modules/$module_repository/$module_name

  sugar-developer-modified $argv[1]

  if test $status -eq 0
    sugar-message bold "$module_repository:$module_name"
    git --no-pager -C $module_path diff --minimal
  end
end

function sugar-developer-diff-all -d 'Display a git diff of all installed modules.'
  for module in (string split " " (sugar-developer-modules))
    set -l module_info (string split ":" $module)
    set -l module_repository $module_info[1]
    set -l module_name $module_info[2]
    sugar-developer-diff $module_repository:$module_name
  end
end
